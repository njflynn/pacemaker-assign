Pacemaker Console - Assignment 1
@authour Neil Flynn 20044801

Notes
======

This version of the project has been converted to a maven project. However I was unable to 
successfully run the project outside of eclipse before the deadline.

Tests can be run in maven (using the command "mvn clean install") and coverage reports are generated
using the maven plugin Jacoco. Coverage reports are visible in the target/code-coverage-reports directory.

Sample users and data have already been stored and are loaded automatically on running the application.

Serialization is in XML by default but can be changed to JSON