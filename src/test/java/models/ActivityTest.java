package models;

import static models.Fixtures.activities;
import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Test;

public class ActivityTest
{ 
  Activity test = new Activity ("walk",  "fridge", 0.001, new DateTime(0l), new Duration(15l));
  Activity test2 = new Activity();

  @Test
  public void testCreate()
  {
    assertEquals ("walk",          test.type);
    assertEquals ("fridge",        test.location);
    assertEquals (0.0001, 0.001,   test.distance);
    assertEquals (new DateTime(0l), test.startTime);
    assertEquals (new Duration(15l), test.duration);
    
    assertNotNull(test2);
    assertNotNull(test2.id);
    assertNotNull(test2.route);
    
    assertNotEquals(test2.hashCode(), test.hashCode());
  }
  
  @Test
  public void equalsTest()
  {
	assertTrue(test.equals( new Activity ("walk",  "fridge", 0.001, new DateTime(0l), new Duration(15l))));
	assertFalse(test.equals( new Activity ("run",  "fridge", 0.001, new DateTime(0l), new Duration(15l))));
	assertFalse(test.equals( new Activity ("walk",  "home", 0.001, new DateTime(0l), new Duration(15l))));
	assertFalse(test.equals( new Activity ("walk",  "fridge", 0.041, new DateTime(0l), new Duration(15l))));
	assertFalse(test.equals( new Activity ("walk",  "fridge", 0.001, new DateTime(1000l), new Duration(15l))));
	assertFalse(test.equals( new Activity ("walk",  "fridge", 0.001, new DateTime(0l), new Duration(14575l))));
	assertFalse(test.equals( new Activity ("cycle",  "shop", 10.500, new DateTime(47895l), new Duration(456l))));
	assertFalse(test.equals( new Activity ()));
	
	assertFalse(test.equals( new User ("name", "lastname", "email", "secret")));
  }

  @Test
  public void testToString()
  {
    assertEquals ("Activity{"+ test.id + ", walk, fridge, 0.001, 1970-01-01T01:00:00.000+01:00, PT0.015S, []}", test.toString());
  }
}