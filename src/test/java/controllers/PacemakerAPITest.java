package controllers;

import static org.junit.Assert.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import models.Activity;
import models.Location;
import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utils.JSONSerializer;
import utils.Serializer;
import utils.XMLSerializer;
import controllers.PacemakerAPI;
import static models.Fixtures.users;
import static models.Fixtures.activities;
import static models.Fixtures.locations;

public class PacemakerAPITest
{
  private PacemakerAPI pacemaker;

  @Before
  public void setup()
  {
	  File  datastoreXML = new File("testdatastore.xml");
	  Serializer serializerXML = new XMLSerializer(datastoreXML);
	    
	  File  datastoreJSON = new File("testdatastore.json");
	  Serializer serializerJSON = new JSONSerializer(datastoreJSON);
	    
	  HashMap<String, Serializer> serializers = new HashMap<>();
	  serializers.put("xml", serializerXML);
	  serializers.put("json", serializerJSON);
	  
	  pacemaker = new PacemakerAPI(serializers);
	  for (User user : users)
	  {
		  pacemaker.createUser(user.firstName, user.lastName, user.email, user.password);
	  }
  }

  @After
  public void tearDown()
  {
    pacemaker = null;
    String datastoreFileXML = "testdatastore.xml";
    deleteFile (datastoreFileXML);
    
    String datastoreFileJSON = "testdatastore.json";
    deleteFile (datastoreFileJSON);
  }

  @Test
  public void testUser()
  {
    assertEquals (users.length, pacemaker.getUsers().size());
    pacemaker.createUser("homer", "simpson", "homer@simpson.com", "secret");
    assertEquals (users.length+1, pacemaker.getUsers().size());
    assertEquals (users[0], pacemaker.getUserByEmail(users[0].email));
  }  
  
  @Test
  public void testUsers()
  {
    assertEquals (users.length, pacemaker.getUsers().size());
    for (User user: users)
    {
      User eachUser = pacemaker.getUserByEmail(user.email);
      assertEquals (user, eachUser);
      assertNotSame(user, eachUser);
    }
  }

  @Test
  public void testDeleteUsers()
  {
    assertEquals (users.length, pacemaker.getUsers().size());
    User marge = pacemaker.getUserByEmail("marge@simpson.com");
    pacemaker.deleteUser(marge.id);
    assertEquals (users.length-1, pacemaker.getUsers().size()); 
    
    pacemaker.deleteUsers();
    assertEquals(0, pacemaker.getUsers().size());
    assertNull(pacemaker.getUser(0l));
  } 
  
  @Test
  public void testAddActivity()
  {
	  User marge = pacemaker.getUserByEmail("marge@simpson.com");
	  Activity activity = pacemaker.createActivity(marge.id, activities[0].type, activities[0].location, activities[0].distance, activities[0].startTime, activities[0].duration);
	  Activity returnedActivity = pacemaker.getActivity(activity.id);
	  assertEquals(activities[0],  returnedActivity);
	  assertNotSame(activities[0], returnedActivity);
  }  
  
  @Test
  public void testAddActivityWithSingleLocation()
  {
    User marge = pacemaker.getUserByEmail("marge@simpson.com");
    Long activityId = pacemaker.createActivity(marge.id, activities[0].type, activities[0].location, activities[0].distance, activities[0].startTime, activities[0].duration).id;

    pacemaker.addLocation(activityId, locations[0].latitude, locations[0].longitude);

    Activity activity = pacemaker.getActivity(activityId);
    assertEquals (1, activity.route.size());
    assertEquals(0.0001, locations[0].latitude,  activity.route.get(0).latitude);
    assertEquals(0.0001, locations[0].longitude, activity.route.get(0).longitude);   
  } 
  
  @Test
  public void testAddActivityWithMultipleLocation()
  {
    User marge = pacemaker.getUserByEmail("marge@simpson.com");
    Long activityId = pacemaker.createActivity(marge.id, activities[0].type, activities[0].location, activities[0].distance, activities[0].startTime, activities[0].duration).id;

    for (Location location : locations)
    {
      pacemaker.addLocation(activityId, location.latitude, location.longitude);      
    }

    Activity activity = pacemaker.getActivity(activityId);
    assertEquals (locations.length, activity.route.size());
    int i = 0;
    for (Location location : activity.route)
    {
      assertEquals(location, locations[i]);
      i++;
    }
  } 
  
  @Test
  public void testSetFileFormat(){
	  	  
	  String response = pacemaker.setFileFormat("xml");
	  assertTrue(response.contains("File format changed to xml"));
	  assertTrue(pacemaker.getSerializer() instanceof XMLSerializer);
	  
	  String response2 = pacemaker.setFileFormat("json");
	  assertTrue(response2.contains("File format changed to json"));
	  assertTrue(pacemaker.getSerializer() instanceof JSONSerializer);
	  
	  String error = pacemaker.setFileFormat("html");
	  assertTrue(error.contains("File format html not supported"));
	  
	  try {
		pacemaker.store();
	} catch (Exception e) {
		e.printStackTrace();
	}
	  
  }
  
  public void deleteFile(String fileName)
  {
    File datastore = new File (fileName);
    if (datastore.exists())
    {
      datastore.delete();
    }
  }
}