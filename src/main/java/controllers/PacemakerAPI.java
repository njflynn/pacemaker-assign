package controllers;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;

import com.google.common.base.Optional;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import utils.Serializer;
import utils.XMLSerializer;
import models.Activity;
import models.Location;
import models.User;

public class PacemakerAPI
{
  private Map<Long, User>     userIndex       = new HashMap<>();
  private Map<String, User>   emailIndex      = new HashMap<>();
  private Map<Long, Activity> activitiesIndex = new HashMap<>();
  private Map<String, Serializer> serializers = new HashMap<>();
      
  private Serializer serializer;
  
  public PacemakerAPI(Map<String, Serializer> serializers)
  {
	  this.serializers = serializers;
	  // xml is default
	  this.serializer = serializers.get("xml");
	  
  }
  
  public PacemakerAPI(Serializer serializer)
  {
	  this.serializer = serializer;
  }
  
  @SuppressWarnings("unchecked")
  public void load() throws Exception
  {
    serializer.read();
    activitiesIndex = (Map<Long, Activity>) serializer.pop();
    emailIndex      = (Map<String, User>)   serializer.pop();
    userIndex       = (Map<Long, User>)     serializer.pop();
    
    // update id to be assigned next user to avoid new users overwriting existing users
    // this is an issue when loading users from file, as loaded users have an id and the static id variable in user class is set to 0
    Set<Long> userIds = userIndex.keySet();
    Long maxUserId = User.counter;
    for (Long value : userIds) {
    	if(value > maxUserId){
    		maxUserId = value;
    	}
     }
    
    User.counter = maxUserId + 1l;
    
    // same issue as with user ids
    Set<Long> activitiesIds = activitiesIndex.keySet();
    Long maxActivityId = Activity.counter;
    for (Long value : activitiesIds) {
    	if(value > maxActivityId){
    		maxActivityId = value;
    	}
     }
    
    Activity.counter = maxActivityId + 1l;
    
  }

  public void store() throws Exception
  {
    serializer.push(userIndex);
    serializer.push(emailIndex);
    serializer.push(activitiesIndex);
    serializer.write(); 
  }
  
  public Collection<User> getUsers ()
  {
    return userIndex.values();
  }
  
  public  void deleteUsers() 
  {
    userIndex.clear();
    emailIndex.clear();
  }
  
  public User createUser(String firstName, String lastName, String email, String password) 
  {
    User user = new User (firstName, lastName, email, password);
    userIndex.put(user.id, user);
    emailIndex.put(email, user);
    return user;
  }
  
  public User getUserByEmail(String email) 
  {
    return emailIndex.get(email);
  }

  public User getUser(Long id) 
  {
    return userIndex.get(id);
  }

  public void deleteUser(Long id) 
  {
    User user = userIndex.remove(id);
    emailIndex.remove(user.email);
  }
  
  public Activity createActivity(Long id, String type, String location, double distance, DateTime start, Duration duration)
  {
    Activity activity = null;
    Optional<User> user = Optional.fromNullable(userIndex.get(id));
    if (user.isPresent())
    {
      activity = new Activity (type, location, distance, start, duration);
      user.get().activities.put(activity.id, activity);
      activitiesIndex.put(activity.id, activity);
    }
    return activity;
  }
  
  public Activity getActivity (Long id)
  {
    return activitiesIndex.get(id);
  }
  
  public void addLocation (Long id, float latitude, float longitude)
  {
    Optional<Activity> activity = Optional.fromNullable(activitiesIndex.get(id));
    if (activity.isPresent())
    {
      activity.get().route.add(new Location(latitude, longitude));
    }
  }
  
  public Serializer getSerializer(){
	  
	  return serializer;
	  
  }
  
public void setSerializer(Serializer newSerializer){
	  serializer = newSerializer;
  }
  
  public String setFileFormat(String format){
	  if(serializers.containsKey(format))
	  {
		  setSerializer(serializers.get(format));
		  return "File format changed to " + format;
	  } else {
		  return "File format " + format + " not supported";
	  }
  }
}
