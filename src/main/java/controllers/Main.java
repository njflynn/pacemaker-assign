package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import utils.JSONSerializer;
import utils.Serializer;
import utils.XMLSerializer;
import models.Activity;
import models.User;
import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;

import com.bethecoder.ascii_table.ASCIITable;
import com.google.common.base.Optional;

public class Main
{
  public PacemakerAPI paceApi;
  
  public Main() throws Exception
  {
	// use xml serialization by default
    File  datastoreXML = new File("datastore.xml");
    Serializer serializerXML = new XMLSerializer(datastoreXML);
    
    File  datastoreJSON = new File("datastore.json");
    Serializer serializerJSON = new JSONSerializer(datastoreJSON);
    
    HashMap<String, Serializer> serializers = new HashMap<>();
    serializers.put("xml", serializerXML);
    serializers.put("json", serializerJSON);
    
    paceApi = new PacemakerAPI(serializers);
    // default is xml
    if (datastoreXML.isFile())
    {
      paceApi.load();
    }
  }
  
  @Command(description="Create a new user")
  public void createUser (@Param(name="first name") String firstName, @Param(name="last name") String lastName, 
                          @Param(name="email")      String email,     @Param(name="password")  String password)
  {
	  Optional<User> newUser = Optional.fromNullable(paceApi.createUser(firstName, lastName, email, password));
    
    if(newUser.isPresent()){
    	System.out.println("User created successfully");
    	User user = newUser.get();
    	String [] header = { "ID", "FIRST NAME", "LAST NAME", "EMAIL", "PASSWORD" };
        String[][] data = { { user.id.toString(), user.firstName, user.lastName, user.email, user.password } };
        
        ASCIITable.getInstance().printTable(header, data);
    	
    }
  }
  
  @Command(description="Get a user's details")
  public void listUser (@Param(name="email") String email)
  {
	  Optional<User> foundUser = Optional.fromNullable(paceApi.getUserByEmail(email));
	  if (foundUser.isPresent()){
		  User user = foundUser.get();
		  String [] header = { "ID", "FIRST NAME", "LAST NAME", "EMAIL", "PASSWORD" };
		  String[][] data = { { user.id.toString(), user.firstName, user.lastName, user.email, user.password } };
    
		  ASCIITable.getInstance().printTable(header, data);
	  } else {
		  System.out.println("No user found with email matching " + email);
	  }
  }
  
  @Command(description="Get a user's details")
  public void listUser (@Param(name="id") long id)
  {
	  Optional<User> foundUser = Optional.fromNullable(paceApi.getUser(id));
	  if (foundUser.isPresent()){
		  User user = foundUser.get();
		  String [] header = { "ID", "FIRST NAME", "LAST NAME", "EMAIL", "PASSWORD" };
		  String[][] data = { { user.id.toString(), user.firstName, user.lastName, user.email, user.password } };
    
		  ASCIITable.getInstance().printTable(header, data);
	  } else {
		  System.out.println("No user found with id matching " + id);
	  }
  }
  
  @Command(description="Get all users' details")
  public void listUsers ()
  {
    Collection<User> users = paceApi.getUsers();
    
    if(users.size() > 0){
	    Iterator<User> iterator = users.iterator();
	 
	    String [] header = { "ID", "FIRST NAME", "LAST NAME", "EMAIL", "PASSWORD" };
	    String[][] data = new String[users.size()][5];
	
	    int row = 0;
	    while (iterator.hasNext()) {
	    	User user = iterator.next();
	    	data[row][0] = user.id.toString();
	    	data[row][1] = user.firstName;
	    	data[row][2] = user.lastName;
	    	data[row][3] = user.email;
	    	data[row][4] = user.password;
	    	row++;
	    }
	    
	    ASCIITable.getInstance().printTable(header, data);
    } else {
    	System.out.println("No users to display");
    }
  }
  
  @Command(description="Delete a User")
  public void deleteUser (@Param(name="id") long id)
  {
    Optional<User> user = Optional.fromNullable(paceApi.getUser(id));
    if (user.isPresent())
    {
      paceApi.deleteUser(user.get().id);
      System.out.println("User " + user.get().id + " deleted");
    }
  }
  
  @Command(description="Add an activity")
  public void addActivity (@Param(name="user-id")  Long   id,       @Param(name="type") String type, 
                           @Param(name="location") String location, @Param(name="distance") double distance,
                           @Param(name="datetime") String start, @Param(name="duration") String duration)
  {
    Optional<User> user = Optional.fromNullable(paceApi.getUser(id));
    if (user.isPresent())
    {
	    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
	    DateTime startTime = formatter.parseDateTime(start);
	    
	    PeriodFormatter durationFormat= new PeriodFormatterBuilder().appendHours().appendSeparator(":").appendMinutes()
	    												.appendSeparator(":").appendSeconds().toFormatter();
	    
	    Period durationPeriod = durationFormat.parsePeriod(duration);
	    Duration durationConverted = durationPeriod.toStandardDuration();
	    
	    System.out.println(durationConverted);
	   
	    Optional<Activity> activity = Optional.fromNullable(paceApi.createActivity(id, type, location, distance, startTime, durationConverted));
	    if(activity.isPresent()){
	    	System.out.println("Activity Successfully created");
	    	Activity newActivity = activity.get();
    	  
	    	String [] header = { "ID", "TYPE", "LOCATION", "DISTANCE", "STARTTIME", "DURATION", "ROUTE" };
	    	String[][] data = { { newActivity.id.toString(), newActivity.type, newActivity.location, String.valueOf(newActivity.distance), 
    		  					newActivity.startTime.toString(), newActivity.duration.toString(), newActivity.route.toString()} };

	    	ASCIITable.getInstance().printTable(header, data);

	    }
    }
  }
  
  @Command(description="Add Location to an activity")
  public void addLocation (@Param(name="activity-id")  Long  id,   
                           @Param(name="latitude")     float latitude, @Param(name="longitude") float longitude)
  {
    Optional<Activity> activity = Optional.fromNullable(paceApi.getActivity(id));
    if (activity.isPresent())
    {
      paceApi.addLocation(activity.get().id, latitude, longitude);
      System.out.println("Location added to activity " + activity.get().id);
    }
  }
  
  @Command(description="List a user's activities")
  public void listActivities(@Param(name="userid") Long userid)
  {
	  Optional<User> foundUser = Optional.fromNullable(paceApi.getUser(userid));
	  if(foundUser.isPresent()){
		  User user = foundUser.get();
		  if(user.activities.size() > 0){
			  Collection<Activity> activities = user.activities.values();
			  
			  Iterator<Activity> iterator = activities.iterator();
			  
			    String [] header = { "ID", "TYPE", "LOCATION", "DISTANCE", "STARTTIME", "DURATION", "ROUTE" };
			    String[][] data = new String[activities.size()][7];
		
			    int row = 0;
			    while (iterator.hasNext()) {
			    	Activity activity = iterator.next();
			    	data[row][0] = activity.id.toString();
			    	data[row][1] = activity.type;
			    	data[row][2] = activity.location;
			    	data[row][3] = String.valueOf(activity.distance);
			    	data[row][4] = activity.startTime.toString();
			    	data[row][5] = activity.duration.toString();
			    	data[row][6] = activity.route.toString();
			    	row++;
			    }
			    
			    ASCIITable.getInstance().printTable(header, data);
		  } else {
		    	System.out.println("No activities to display for user " + userid);
		  	}
	  } else {
		  System.out.println("No user found with id matching " + userid);
	  }
	  
  }
  
  @Command(description="List a user's activities, sorted by type, location, distance, date or duration")
  public void listActivities(@Param(name="userid") Long userid, @Param(name="sortyBy: attribute") String sortBy)
  {
	  Optional<User> foundUser = Optional.fromNullable(paceApi.getUser(userid));
	  if(foundUser.isPresent()){
		  User user = foundUser.get();
	  if(user.activities.size() > 0){
		  Collection<Activity> activities = user.activities.values();
		  
		  String [] header = { "ID", "TYPE", "LOCATION", "DISTANCE", "STARTTIME", "DURATION", "ROUTE" };
		  String[][] data = new String[activities.size()][7];
		  
		  int row = 0;
		  Iterator<Activity> iteratorActivities = activities.iterator();
		  
		  switch(sortBy){
		  	case "type":
		  		Set<String> sortedTypes = new TreeSet<>();
		  		for(Activity activity : activities){
		  			sortedTypes.add(activity.type);
		  		}
		  		
		  		Iterator<String> iteratorTypes = sortedTypes.iterator();
		  		row = 0;
		  		while (iteratorTypes.hasNext()) {
		  			String type = iteratorTypes.next();
		  			iteratorActivities = activities.iterator();
		  			
		  			while(iteratorActivities.hasNext()) {
		  				Activity activity = iteratorActivities.next();
		  				if(activity.type.equals(type)){
			  			  data[row][0] = activity.id.toString();
						  data[row][1] = activity.type;
						  data[row][2] = activity.location;
						  data[row][3] = String.valueOf(activity.distance);
						  data[row][4] = activity.startTime.toString();
						  data[row][5] = activity.duration.toString();
						  data[row][6] = activity.route.toString();
						  row++;
		  			}
		  		}
		  		}
		  		break;
		  	case "location":
		  		Set<String> sortedLocations = new TreeSet<>();
		  		for(Activity activity : activities){
		  			sortedLocations.add(activity.location);
		  		}
		  		
		  		Iterator<String> iteratorLocations = sortedLocations.iterator();
		  		row = 0;
		  		while (iteratorLocations.hasNext()) {
		  			String location = iteratorLocations.next();
		  			iteratorActivities = activities.iterator();
		  			
		  			while(iteratorActivities.hasNext()) {
		  				Activity activity = iteratorActivities.next();
		  				if(activity.location.equals(location)){
			  			  data[row][0] = activity.id.toString();
						  data[row][1] = activity.type;
						  data[row][2] = activity.location;
						  data[row][3] = String.valueOf(activity.distance);
						  data[row][4] = activity.startTime.toString();
						  data[row][5] = activity.duration.toString();
						  data[row][6] = activity.route.toString();
						  row++;
		  			}
		  		}
		  		}
		  		break;
		  	case "distance":
		  		Set<Double> sortedDistances = new TreeSet<>();
		  		for(Activity activity : activities){
		  			sortedDistances.add(activity.distance);
		  		}
		  		
		  		Iterator<Double> iteratorDistances = sortedDistances.iterator();
		  		row = 0;
		  		while (iteratorDistances.hasNext()) {
		  			double distance = iteratorDistances.next();
		  			iteratorActivities = activities.iterator();
		  			
		  			while(iteratorActivities.hasNext()) {
		  				Activity activity = iteratorActivities.next();
		  				if(activity.distance == distance){
			  			  data[row][0] = activity.id.toString();
						  data[row][1] = activity.type;
						  data[row][2] = activity.location;
						  data[row][3] = String.valueOf(activity.distance);
						  data[row][4] = activity.startTime.toString();
						  data[row][5] = activity.duration.toString();
						  data[row][6] = activity.route.toString();
						  row++;
		  			}
		  		}
		  		}
		  		break;
		  	case "date":
		  		Set<DateTime> sortedStartTimes = new TreeSet<>();
		  		for(Activity activity : activities){
		  			sortedStartTimes.add(activity.startTime);
		  		}
		  		
		  		Iterator<DateTime> iteratorStartTimes = sortedStartTimes.iterator();
		  		row = 0;
		  		while (iteratorStartTimes.hasNext()) {
		  			DateTime startTime = iteratorStartTimes.next();
		  			iteratorActivities = activities.iterator();
		  			
		  			while(iteratorActivities.hasNext()) {
		  				Activity activity = iteratorActivities.next();
		  				if(activity.startTime == startTime){
			  			  data[row][0] = activity.id.toString();
						  data[row][1] = activity.type;
						  data[row][2] = activity.location;
						  data[row][3] = String.valueOf(activity.distance);
						  data[row][4] = activity.startTime.toString();
						  data[row][5] = activity.duration.toString();
						  data[row][6] = activity.route.toString();
						  row++;
		  			}
		  		}
		  		}
		  		break;
		  	case "duration":
		  		Set<Duration> sortedDurations = new TreeSet<>();
		  		for(Activity activity : activities){
		  			sortedDurations.add(activity.duration);
		  		}
		  		
		  		Iterator<Duration> iteratorDurations = sortedDurations.iterator();
		  		row = 0;
		  		while (iteratorDurations.hasNext()) {
		  			Duration duration = iteratorDurations.next();
		  			iteratorActivities = activities.iterator();
		  			
		  			while(iteratorActivities.hasNext()) {
		  				Activity activity = iteratorActivities.next();
		  				if(activity.duration == duration){
			  			  data[row][0] = activity.id.toString();
						  data[row][1] = activity.type;
						  data[row][2] = activity.location;
						  data[row][3] = String.valueOf(activity.distance);
						  data[row][4] = activity.startTime.toString();
						  data[row][5] = activity.duration.toString();
						  data[row][6] = activity.route.toString();
						  row++;
		  			}
		  		}
		  		}
		  		break;
		  	default:
		  		System.out.println("Cannot sort activities by " + sortBy);
		  		break;
		  }
		    
		   ASCIITable.getInstance().printTable(header, data);
	    
  		} else {
	    	System.out.println("No activities to display for user " + userid);
  		}
  } else {
	  System.out.println("No user found with id matching " + userid);
  }
  }
  
  @Command(description="Change format of data stored")
  public void changeFileFormat(@Param(name="format") String fileFormat)
  {
	  System.out.println(paceApi.setFileFormat(fileFormat));
  }
  
  @Command
  public void load()
  {
	  try {
		paceApi.load();
		System.out.println("Load was successful");
	} catch (Exception e) {
		System.out.println("Error loading");
	}
  }
  
  @Command
  public void store()
  {
	  try {
		paceApi.store();
		System.out.println("Saved successfully");
	} catch (Exception e) {
		System.out.println("Error storing");
	}
  }
  
  public static void main(String[] args) throws Exception
  {
    Main main = new Main();

    Shell shell = ShellFactory.createConsoleShell("pm", "Welcome to pacemaker-console - ?help for instructions", main);
    shell.commandLoop();
    
    main.paceApi.store();
  }
}
