package models;

import static com.google.common.base.Objects.toStringHelper;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.google.common.base.Objects;

public class Activity
{ 
  public static Long   counter = 0l;
  
  public Long   id;
  
  public String type;
  public String location;
  public double distance;
  public DateTime startTime;
  public Duration duration;
  
  public List<Location> route;
 
  public Activity()
  {
	  this.id        = counter++;
	  route = new ArrayList<>();
  }
  
  public Activity(String type, String location, double distance, DateTime startTime, Duration duration)
  {
    this.id        = counter++;
    this.type      = type;
    this.location  = location;
    this.distance  = distance;
    this.startTime = startTime;
    this.duration = duration;
    route = new ArrayList<>();
  }
  
  @Override
  public String toString()
  {
    return toStringHelper(this).addValue(id)
                               .addValue(type)
                               .addValue(location)
                               .addValue(distance)
                               .addValue(startTime)
                               .addValue(duration)
                               .addValue(route)
                               .toString();
  }
  
  @Override  
  public int hashCode()  
  {  
     return Objects.hashCode(this.id, this.type, this.location, this.distance, this.startTime, this.duration, this.route);  
  } 
  
  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Activity)
    {
      final Activity other = (Activity) obj;
      return Objects.equal(type, other.type) 
          && Objects.equal(location,  other.location)
          && Objects.equal(distance,  other.distance)
          && Objects.equal(startTime, other.startTime)
          && Objects.equal(duration, other.duration)
          && Objects.equal(route,     other.route);    
    }
    else
    {
      return false;
    }
  }
}